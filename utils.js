
let firstname_weapon = ['destructeur', 'mangeur', 'découpeur', 'trancheur']
let lastname_weapon = [' de poule', ' de démon', ' de fleur', ' de saucisson']

let firstname = ['Albert', 'Robert', 'José', 'Martin', 'Hector', 'Maurice', 'Squoicenom', 'Thomas', 'Tom']
let lastname = [' Boule', ' Cheval', ' Groscon', ' Ptimerdeu', ' Vicelard', ' Rentrecheztoi', ' Limortel', ' Lecouillu', 'Lamolle']


let actionColor = ['red', 'orange', 'green', 'blue', 'purple']

function entierAleatoire(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

function logIt(action, info, typeAction){
    let theDiv = document.getElementById("infos_jeu");
    let actionText = document.createElement('span');
    actionText.innerHTML = action + ' : ';
    actionText.style.color = actionColor[typeAction];
    let infoText = document.createElement('span');
    infoText.innerHTML = info;
    let jumpLine = document.createElement('br');
    theDiv.appendChild(actionText);
    theDiv.appendChild(infoText);
    theDiv.appendChild(jumpLine);
}
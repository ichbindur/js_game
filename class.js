class Personnage {
    
    constructor(vie = 150, force = 15, name = 'default_name', defense = 10){
        this.vie = vie ;
        this.force = force;
        this.name = name;
        this.defense = defense;
        logIt('instanciation personnage', 'le personnage ' + this.name + ' entre sur le terrain', 2)
        this.arme = new Arme();
        this.estMort = false;
    }

    estVivant = function(){
        if(this.vie <= 0){
            this.estMort = true;
            return false;
        }else{
            return true;
        }
    }

    frappeUnEnnemi = function (ennemi){
        let dice = entierAleatoire(1, 20);
        let hit = dice + this.arme.damages; 
        if(dice == 1 ){
            logIt('frappeUnEnnemi', this.name + ' à loupé son attaque', 1);
        }else{
            let damages = hit - ennemi.defense;
            if(damages >= 0){
                ennemi.vie -= damages;
                logIt('frappeUnEnnemi', this.name + ' à infligé ' + damages + ' à ' + ennemi.name, 1);
            }
            else {
                logIt('frappeUnEnnemi', this.name + ' à loupé son attaque', 1);
            }
        }
        if(ennemi.vie <= 0){
            ennemi.mourrir();
        }
    }

    mourrir = function () {
        logIt('mourrir', this.name + ' est mort tout le monde est triste', 0)
        this.estMort = true
    }
}


class Arme {
    constructor(){
        this.damages = entierAleatoire(10,80);
        this.name = firstname_weapon[entierAleatoire(0,3)] + lastname_weapon[entierAleatoire(0,3)];
        logIt('loot d arme', 'L arme légendaire : ' + this.name + ' à été looté avec ' + this.damages + 'dégats', 3);
    }
}

class LeJeu {

    constructor(nombreDeJoueur){
        this.lesJoueurs = []
        this.ilResteDesJoueurs = true
        this.nombreDeJoueur = nombreDeJoueur
        this.genererLesJoueurs()
    }

    genererLesJoueurs = function(){
        for(let i = 0; i < this.nombreDeJoueur; i++){
            let vie = entierAleatoire(80, 150)
            let force = entierAleatoire(5, 25)
            let name = firstname[entierAleatoire(0,8)] + lastname[entierAleatoire(0,8)] 
            let defense = entierAleatoire(1,40)
            this.lesJoueurs.push(new Personnage(vie, force, name, defense))
        }
    }

    jouer = function(){
        let tempPlayers = this.lesJoueurs
        let nbJoueur = this.nombreDeJoueur
        while(this.ilResteDesJoueurs){
            let attaquant = tempPlayers[entierAleatoire(0, nbJoueur - 1)]
            tempPlayers = tempPlayers.filter(o => o != attaquant)
            let peuchere = tempPlayers[entierAleatoire(0, nbJoueur - 2)]
            attaquant.frappeUnEnnemi(peuchere)
            
            tempPlayers = this.lesJoueurs.filter(o => !o.estMort)
            nbJoueur = tempPlayers.length
            if(nbJoueur == 1){
                this.ilResteDesJoueurs = false
            }
        }
        logIt('Nous avons un gagnant !', tempPlayers[0].name + ' à gagné le game!', 4)
        console.log('tempPlayers', tempPlayers)
    }


}